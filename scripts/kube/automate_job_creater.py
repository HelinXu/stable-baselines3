import yaml
import os


def args(seed, i, exp, proj):
    return [f"""
cd /home/helin/Code/dexsuite/stable-baselines3 &&
pip3 install wandb==0.12.21 &&
pip3 install moviepy h5py pybullet icecream && 
export WANDB_API_KEY=8c78f489ad891682c3ca2621e1435b504d8ba309 && 
export PYTHONPATH=`pwd` && 
export OMP_NUM_THREADS=4 && 
export OPENBLAS_NUM_THREADS=4 && 
export MKL_NUM_THREADS=6 && 
export VECLIB_MAXIMUM_THREADS=4 && 
export NUMEXPR_NUM_THREADS=6 && 
python3 -u main/train_ppo_faucet_pc_img.py --exp {exp} --seed {seed} --iter 5000 --object_name faucet --lr 0.0001 --bs 512 --workers 5 --index_group top --project {proj} --use_seg --pn_version 1
"""]
# python3 -u main/train_ppo_faucet_pc_img.py --exp local --seed 100 --iter 5000 --object_name faucet --lr 0.0001 --bs 512 --workers 1 --index_group top --project debug --use_seg --pn_version 1
# python3 -u main/train_faucet_rl_state.py --exp {exp} --n 100 --index {i} --seed {seed} --iter 1500 --object_name faucet --lr 0.0001 --bs 512 --workers 10
# python3 -u main/train_ppo_faucet_pc.py --exp {exp} --index {i} --seed {seed} --iter 2000 --object_name faucet --lr 0.0001 --bs 512 --workers 10

def create_job(job_name, job_file, seed, id, exp, proj):
    with open(job_file, 'r') as stream:
        job_data = yaml.load(stream, Loader=yaml.FullLoader)
    job_data['metadata']['name'] = job_name
    job_data['spec']['template']['spec']['containers'][0]['args'] = args(seed=seed, i=id, exp=exp, proj=proj)
    return job_data


def delete_jobs(job_name_pref, ids, names=None):
    if names is None:
        for i in ids:
            os.system('kubectl delete job ' + job_name_pref + str(i))
    else:
        for name in names:
            os.system('kubectl delete job ' + name)


def create_jobs(  # NOTE: get all parameters here!
        job_name_pref='helin-pc-noimg-seg-pn1-camfaucet2-teacher1-pose3-5-',
        project='segmentation',
        ids=range(0, 1),
        seed=100,
        seeds=[100, 200, 300, 400, 500],
        exp='seg5-pn1',
        job_file='template.yaml'):
    # for i in ids:
    #     with open('tmp.yaml', 'w') as file:
    #         dump = yaml.dump(create_job(job_name=job_name_pref + str(i), job_file=job_file, seed=seed, id=i, exp=exp))
    #         file.write(dump)
    #     os.system('kubectl create -f tmp.yaml')
    for seed in seeds:
        for i in ids:
            with open('tmp.yaml', 'w') as file:
                dump = yaml.dump(create_job(job_name=job_name_pref + str(seed), job_file=job_file, seed=seed, id=i, exp=exp, proj=project))
                file.write(dump)
            os.system('kubectl create -f tmp.yaml')


# delete_jobs(job_name_pref='helin-pc-noimg-seg-camfaucet2-teacher1-pose3-6-', ids=range(500, 700, 100),
#             names=[
#                 # 'helin-pc-noimg-seg-camfaucet2-teacher1-pose3-6-100',
#                 # 'helin-pc-img-noseg-camfaucet2-teacher1-pose3-6-100',
#                 # 'helin-pc-img-noseg-camfaucet2-teacher1-pose3-6-400',
#                 # 'helin-pc-noimg-noseg-camfaucet2-teacher1-pose3-6-300',
#                 # 'helin-pc-noimg-noseg-camfaucet2-teacher1-pose3-6-500',
#                 # 'helin-pc-noimg-seg-camfaucet2-teacher1-pose3-6-100',
#                 # 'helin-pc-noimg-seg-camfaucet2-teacher1-pose3-6-300',
#                 'helin-pc-noimg-seg-camfaucet2-teacher1-pose3-6-500',
#             ])
create_jobs(ids=range(0, 1))
# os.system('rm tmp.yaml')
