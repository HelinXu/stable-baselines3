import os

with open('delete_jobs.txt') as f:
    lines = f.readlines()
lines = [line.strip() for line in lines]
for line in lines:
    job_name = line.split(' ')[0]
    os.system('kubectl delete job ' + job_name)
