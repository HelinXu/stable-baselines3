import yaml
import os


def args(seed, exp, proj, seg, img, num_workers=5, pn_version=1, index=-1):
    return [f"""
cd /home/helin/Code/dexsuite/stable-baselines3 &&
pip3 install wandb==0.12.21 &&
pip3 install moviepy h5py pybullet icecream && 
export WANDB_API_KEY=8c78f489ad891682c3ca2621e1435b504d8ba309 && 
export PYTHONPATH=`pwd` && 
export OMP_NUM_THREADS=4 && 
export OPENBLAS_NUM_THREADS=4 && 
export MKL_NUM_THREADS=6 && 
export VECLIB_MAXIMUM_THREADS=4 && 
export NUMEXPR_NUM_THREADS=6 && 
python3 -u main/train_ppo_faucet_pc_img.py --exp {exp} --seed {seed} --iter 5000 --object_name faucet --lr 0.0001 --bs 512 --workers {num_workers} {'--index_group top' if index==-1 else f'--index {index}'} --project {proj} {'--use_seg' if seg else ''} {f'--pn_version {pn_version}' if pn_version>=0 else ''} {'--use_img' if img else ''}
"""]
# python3 -u main/train_ppo_faucet_pc_img.py --exp debug --seed 100 --iter 5000 --object_name faucet --lr 0.0001 --bs 512 --workers 5 --index 1

def create_job(job_name, job_file, seed, exp, proj, seg, img, pn_version, num_workers, index):
    with open(job_file, 'r') as stream:
        job_data = yaml.load(stream, Loader=yaml.FullLoader)
    job_data['metadata']['name'] = job_name
    job_data['spec']['template']['spec']['containers'][0]['args'] = args(seed=seed, exp=exp, proj=proj, seg=seg, img=img, num_workers=num_workers, pn_version=pn_version, index=index)
    return job_data


def delete_jobs(job_name_pref, ids, names=None):
    if names is None:
        for i in ids:
            os.system('kubectl delete job ' + job_name_pref + str(i))
    else:
        for name in names:
            os.system('kubectl delete job ' + name)


def create_jobs(  # NOTE: get all parameters here!
        job_name_pref='helin-noseg-noimg-5workers-',
        project='seg',
        seeds=[100, 200, 300, 400, 500],
        exp='nautilus',
        job_file='template.yaml',
        seg=False,
        img=False,
        pn_version=1,
        num_workers=5,
        index=-1,
):
    for seed in seeds:
        with open('tmp.yaml', 'w') as file:
            dump = yaml.dump(create_job(job_name=job_name_pref + str(seed), job_file=job_file, seed=seed, exp=exp, proj=project, seg=seg, img=img, num_workers=num_workers, pn_version=pn_version, index=index))
            file.write(dump)
        os.system('kubectl create -f tmp.yaml')


create_jobs(job_name_pref='helin-noseg-noimg-4workers-pn-nobn-', project='seg', seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=False, img=False, num_workers=4, pn_version=-1)

create_jobs(job_name_pref='helin-noseg-img-4workers-pn-nobn-', project='seg', seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=False, img=True, num_workers=4, pn_version=-1)

create_jobs(job_name_pref='helin-seg-noimg-4workers-pn-nobn-', project='seg', seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=4, pn_version=-1)

create_jobs(job_name_pref='helin-seg-noimg-4workers-pn1-nobn-', project='seg', seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=4, pn_version=1)

create_jobs(job_name_pref='helin-seg-noimg-4workers-pn2-nobn-', project='seg', seeds=[100, 200, 300], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=4, pn_version=2)

create_jobs(job_name_pref='helin-seg-noimg-4workers-pn3-nobn-', project='seg', seeds=[100, 200, 300], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=4, pn_version=3)

# single object overfitting
create_jobs(job_name_pref='helin-0-seg-noimg-5workers-pn3-nobn-', project='seg', index=0, seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=5, pn_version=2)
create_jobs(job_name_pref='helin-20-seg-noimg-5workers-pn3-nobn-', project='seg', index=20, seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=5, pn_version=2)
create_jobs(job_name_pref='helin-10-seg-noimg-5workers-pn3-nobn-', project='seg', index=10, seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=5, pn_version=2)
create_jobs(job_name_pref='helin-11-seg-noimg-5workers-pn3-nobn-', project='seg', index=11, seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=5, pn_version=2)
create_jobs(job_name_pref='helin-19-seg-noimg-5workers-pn3-nobn-', project='seg', index=19, seeds=[100, 200], exp='nautilus', job_file='template.yaml', seg=True, img=False, num_workers=5, pn_version=2)
