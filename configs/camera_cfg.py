import numpy as np

relocate_camera = {
    "relocate_view": dict(position=np.array([-0.4, 0.4, 0.6]), look_at_dir=np.array([0.4, -0.4, -0.6]),
                            right_dir=np.array([-1, -1, 0]), fov=np.deg2rad(69.4), resolution=(64, 64))
    }