import numpy as np

from hand_env_utils.teleop_env import create_faucet_teacher_env

from icecream import ic, install
install()
ic.configureOutput(includeContext=True, contextAbsPath=True, prefix='File ')

if __name__ == '__main__':
    device = "cuda:0"
    split = 'train'
    for id in [0, 10, 11, 19, 20, 24, 26, 27, 28, 8, 34, 38, 41, 42, 44, 47, 53, 55, 61]:
        ic(id)

        pc_data = []

        use_img = False
        use_seg = True
        save_data = False
        n_fold = 1
        env = create_faucet_teacher_env(object_name='faucet',
                                        use_visual_obs=True,
                                        use_gui=True,
                                        is_eval=True,
                                        pc_noise=True,
                                        use_img=use_img,
                                        pc_seg=use_seg,
                                        friction=1,
                                        index=id)

        obs = env.reset()
        for i in range(env.horizon * n_fold):
            if obs is None:
                print(f"{split}_{i}")
                continue
            action = np.random.uniform(-2, 2, size=env.action_space.shape)
            obs, reward, done, _ = env.step(action)
            pc_data.append(obs["faucet_1-pc_seg" if use_seg else "faucet_1-point_cloud"])
            env.scene.update_render()

            if i % env.horizon == 0:
                obs = env.reset()

        # save data
        if save_data:
            if len(pc_data) == env.horizon * n_fold:
                np.save(f"data/faucet_seg_data/1{split}_{id}.npy", pc_data)
            else:  # this means some frames are missing. (due to a bug in sapien)
                np.save(f"data/faucet_seg_data/0{split}_{id}.npy", pc_data)

