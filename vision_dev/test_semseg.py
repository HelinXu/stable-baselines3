import argparse
import os
# from data_utils.S3DISDataLoader import ScannetDatasetWholeScene
# from data_utils.indoor3d_util import g_label2color
from data_utils import SemSegDataset
import torch
import logging
from pathlib import Path
import sys
import importlib
from tqdm import tqdm
# import provider
import numpy as np

g_label2color = {
    -1: [255, 0, 0],
    0: [0, 255, 0],
    1: [0, 0, 255],
    2: [220, 220, 0],
    3: [0, 255, 255],
}

BASE_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = BASE_DIR
sys.path.append(os.path.join(ROOT_DIR, 'models'))

classes = ['handle', 'faucet', 'hand', 'arm']
class2label = {cls: i for i, cls in enumerate(classes)}
seg_classes = class2label
seg_label_to_cat = {}
for i, cat in enumerate(seg_classes.keys()):
    seg_label_to_cat[i] = cat


def parse_args():
    '''PARAMETERS'''
    parser = argparse.ArgumentParser('Model')
    parser.add_argument('--gpu', type=str, default='0', help='specify gpu device')
    parser.add_argument('--num_point', type=int, default=4096, help='point number [default: 4096]')
    parser.add_argument('--log_dir', type=str, required=True, help='experiment root')
    parser.add_argument('--split', type=str, default='val', help='dataset split [default: val]')
    return parser.parse_args()


def save_ply(points, seg, filename):
    # print(points)
    points = points.transpose(1, 0)
    with open(filename, 'w') as f:
        f.write('ply\n')
        f.write('format ascii 1.0\n')
        f.write('element vertex %d\n' % points.shape[0])
        f.write('property float x\n')
        f.write('property float y\n')
        f.write('property float z\n')
        f.write('property uchar red\n')
        f.write('property uchar green\n')
        f.write('property uchar blue\n')
        f.write('end_header\n')
        for p in range(points.shape[0]):
            if seg[p] >= 0:
                color = g_label2color[seg[p]]
                f.write('%f %f %f %d %d %d\n' % (points[p, 0], points[p, 1], points[p, 2], color[0], color[1], color[2]))
            else:
                color = g_label2color[seg[p]+100]
                f.write('%f %f %f %d %d %d\n' % (points[p, 0], points[p, 1], points[p, 2], color[0], color[1], color[2]))
                f.write('%f %f %f %d %d %d\n' % (points[p, 0]+0.001, points[p, 1], points[p, 2], 200, 0, 0))


def main(args):
    def log_string(str):
        logger.info(str)
        print(str)

    '''HYPER PARAMETER'''
    os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu
    experiment_dir = 'log/sem_seg/' + args.log_dir
    visual_dir = experiment_dir + '/visual/'
    visual_dir = Path(visual_dir)
    visual_dir.mkdir(exist_ok=True)

    '''LOG'''
    args = parse_args()
    logger = logging.getLogger("Model")
    logger.setLevel(logging.INFO)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    file_handler = logging.FileHandler('%s/eval.txt' % experiment_dir)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    log_string('PARAMETER ...')
    log_string(args)

    NUM_CLASSES = 4
    split = args.split

    TEST_DATASET = SemSegDataset(split=split)
    log_string("The number of test data is: %d" % len(TEST_DATASET))

    '''MODEL LOADING'''
    model_name = os.listdir(experiment_dir + '/logs')[0].split('.')[0]
    MODEL = importlib.import_module(model_name)
    classifier = MODEL.get_model(NUM_CLASSES).cuda()
    checkpoint = torch.load(str(experiment_dir) + '/checkpoints/best_model.pth')
    classifier.load_state_dict(checkpoint['model_state_dict'])
    classifier = classifier.eval()

    with torch.no_grad():
        num_batches = len(TEST_DATASET)
        total_correct = 0

        total_seen_class = [0 for _ in range(NUM_CLASSES)]
        total_correct_class = [0 for _ in range(NUM_CLASSES)]
        total_iou_deno_class = [0 for _ in range(NUM_CLASSES)]

        log_string('---- EVALUATION WHOLE SCENE----')

        for batch_idx in tqdm(range(num_batches)):
            # print("Inference [%d/%d] %s ..." % (batch_idx + 1, num_batches, 0))
            total_seen_class_tmp = [0 for _ in range(NUM_CLASSES)]
            total_correct_class_tmp = [0 for _ in range(NUM_CLASSES)]
            total_iou_deno_class_tmp = [0 for _ in range(NUM_CLASSES)]

            points, target = TEST_DATASET[batch_idx]
            points = points.reshape((1, 512, 3))
            points, target = points.float().cuda(), target.long().cuda()
            points = points.transpose(2, 1)

            seg_pred, trans_feat = classifier(points)
            pred_val = seg_pred.contiguous().cpu().data.numpy()

            batch_label = target.cpu().data.numpy()

            pred_val = np.argmax(pred_val, 2)

            correct = np.sum((pred_val == batch_label))

            total_correct += correct
            tmp, _ = np.histogram(batch_label, range(NUM_CLASSES + 1))

            for l in range(NUM_CLASSES):
                total_seen_class[l] += np.sum((batch_label == l))
                total_correct_class[l] += np.sum((pred_val == l) & (batch_label == l))
                total_iou_deno_class[l] += np.sum(((pred_val == l) | (batch_label == l)))

            score = correct / (pred_val[0].shape[0])
            color_map = pred_val[0]
            color_map[(pred_val != batch_label)[0]] -= 100
            if score < 0.99:
                save_ply(np.array(points.cpu())[0], color_map,
                         os.path.join(visual_dir, split + str(batch_idx) + '_' + str(int(score * 100)) + '.ply'))

        iou_map = np.array(total_correct_class_tmp) / (np.array(total_iou_deno_class_tmp, dtype=float) + 1e-6)
        print(iou_map)
        arr = np.array(total_seen_class_tmp)
        tmp_iou = np.mean(iou_map[arr != 0])
        log_string('Mean IoU of %s: %.4f' % (0, tmp_iou))
        print('----------------------------')

        IoU = np.array(total_correct_class) / (np.array(total_iou_deno_class, dtype=float) + 1e-6)
        iou_per_class_str = '------- IoU --------\n'
        for l in range(NUM_CLASSES):
            iou_per_class_str += 'class %s, IoU: %.3f \n' % (
                seg_label_to_cat[l] + ' ' * (14 - len(seg_label_to_cat[l])),
                total_correct_class[l] / float(total_iou_deno_class[l]))
        log_string(iou_per_class_str)
        log_string('eval point avg class IoU: %f' % np.mean(IoU))
        log_string('eval whole scene point avg class acc: %f' % (
            np.mean(np.array(total_correct_class) / (np.array(total_seen_class, dtype=np.float) + 1e-6))))
        log_string('eval whole scene point accuracy: %f' % (
                np.sum(total_correct_class) / float(np.sum(total_seen_class) + 1e-6)))

        print("Done!")

        # start to visualize
        import open3d as o3d
        import glob
        def view_ply(filename='0_gt.ply'):
            pc = o3d.io.read_point_cloud(filename)
            o3d.visualization.draw_geometries([pc])

        for f in glob.glob(os.path.join(visual_dir, split + '*.ply')):
            print(f)
            view_ply(f)


if __name__ == '__main__':
    args = parse_args()
    main(args)
