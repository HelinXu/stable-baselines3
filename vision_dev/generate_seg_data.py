import numpy as np

from hand_env_utils.teleop_env import create_faucet_teacher_env
from stable_baselines3.ppo import PPO
import cv2
from hand_teleop.utils.camera_utils import fetch_texture, generate_imagination_pc_from_obs
from stable_baselines3.common.torch_layers import PointNetExtractor, PointNetImaginationExtractor
import torch.nn as nn
import os
from icecream import ic, install
install()

if __name__ == '__main__':
    for ep in [250]:
        checkpoint_path = f"../checkpoints/model_{ep}.zip"
        device = "cuda:0"

        test_data = True

        if test_data:
            for id in [0, 10, 11, 19, 20, 24, 26, 27, 28, 8, 34, 38, 41, 42, 44, 47, 53, 55, 61]:
                if os.path.isfile(f"data/faucet_seg_data/test_{ep}_{id}.npy"):
                    print(f'skipping test_{ep}_{id}.npy')
                    continue

                elif os.path.isfile(f"data/faucet_seg_data/0test_{ep}_{id}.npy"):
                    os.system(f'rm data/faucet_seg_data/0test_{ep}_{id}.npy')

                pc_data = []
                # video = []
                use_img = False
                use_seg = True
                pointnet_version = -1
                env = create_faucet_teacher_env(object_name='faucet',
                                                use_visual_obs=True,
                                                use_gui=True,
                                                is_eval=True,
                                                pc_noise=True,
                                                use_img=use_img,
                                                pc_seg=use_seg,
                                                friction=1,
                                                index=id)
                feature_extractor_class = PointNetImaginationExtractor if use_img else PointNetExtractor
                feature_extractor_kwargs = {
                    "pc_key": "faucet_1-pc_seg" if use_seg else "faucet_1-point_cloud",
                    "local_channels": (64, 128, 256),
                    "global_channels": (256,),
                    "use_bn": False,
                }
                if use_img:
                    feature_extractor_kwargs["imagination_keys"] = ('imagination_robot', 'imagination_faucet',)
                if pointnet_version != -1:
                    feature_extractor_kwargs["version"] = pointnet_version

                policy_kwargs = {
                    "features_extractor_class": feature_extractor_class,
                    "features_extractor_kwargs": feature_extractor_kwargs,
                    "net_arch": [dict(pi=[64, 64], vf=[64, 64])],
                    "activation_fn": nn.ReLU,
                }

                # ic(policy_kwargs)
                policy = PPO.load(checkpoint_path, env, device, policy_kwargs=policy_kwargs)
                # reward_sum = 0

                # ic(id)
                obs = env.reset()
                for i in range(env.horizon):
                    if obs is None: continue
                    action = policy.predict(observation=obs, deterministic=True)[0]
                    obs, reward, done, _ = env.step(action)
                    pc_label = obs['faucet_1-pc_seg']
                    pc_data.append(pc_label)

                    env.scene.update_render()

                # save data
                if len(pc_data) == env.horizon:
                    np.save(f"data/faucet_seg_data/test_{ep}_{id}.npy", pc_data)
                else:
                    np.save(f"data/faucet_seg_data/0test_{ep}_{id}.npy", pc_data)