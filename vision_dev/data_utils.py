from torch.utils.data import Dataset
from tqdm import tqdm
import numpy as np
from os.path import join as pjoin
import torch


# train set: data/faucet_seg_data/train.npy
class SemSegDataset(Dataset):
    def __init__(self, root_dir='data/faucet_seg_data', split='train'):
        self.root_dir = root_dir
        self.split = split
        self.data = self.load_data()
        self.labelweights = np.ones(4)

    def load_data(self):
        data = np.load(pjoin(self.root_dir, f'{self.split}.npy'), 'r')
        return data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        sample = self.data[idx]
        points = sample[..., 0:3]
        labels = np.argmax(sample[..., 3:], axis=1)
        return torch.tensor(points), torch.tensor(labels)


# test
if __name__ == '__main__':
    dataset = SemSegDataset(split='train')
    for i in tqdm(range(len(dataset))):
        sample = dataset[i]
        print(sample)
