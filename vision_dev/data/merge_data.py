import numpy as np
from glob import glob
import os

if __name__ == "__main__":
    dataset = []
    for f in glob('faucet_seg_data/val_*.npy'):
        data = np.load(f)
        dataset.append(data)
        print(f)
    # save dataset
    dataset = np.concatenate(dataset, axis=0)
    np.save(os.path.join('faucet_seg_data', 'val.npy'), dataset)

    dataset = []
    for f in glob('faucet_seg_data/train_*.npy'):
        data = np.load(f)
        dataset.append(data)
        print(f)
    # save dataset
    dataset = np.concatenate(dataset, axis=0)
    np.save(os.path.join('faucet_seg_data', 'train.npy'), dataset)

    dataset = []
    for f in glob('faucet_seg_data/test_*.npy'):
        data = np.load(f)
        dataset.append(data)
        print(f)
    # save dataset
    dataset = np.concatenate(dataset, axis=0)
    np.save(os.path.join('faucet_seg_data', 'test.npy'), dataset)