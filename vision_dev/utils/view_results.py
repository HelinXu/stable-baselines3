import open3d as o3d
import glob


def view_ply(filename='0_gt.ply'):
    pc = o3d.io.read_point_cloud(filename)
    o3d.visualization.draw_geometries([pc])


if __name__ == '__main__':
    for f in glob.glob('*.ply'):
        print(f)
        view_ply(f)
