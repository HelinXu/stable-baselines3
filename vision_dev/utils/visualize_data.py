import numpy as np
import open3d as o3d
import matplotlib.pyplot as plt
from icecream import ic, install
install()

if __name__ == '__main__':
    data = np.load('../data/faucet_seg_data/train.npy', 'r')
    for _ in range(10):
        i = np.random.randint(0, len(data))
        pc = data[i]
        labels = pc[..., 3] * 1 + pc[..., 4] * 2 + pc[..., 5] * 3 + pc[..., 6] * 4
        colors = plt.get_cmap("tab20")(labels / 4).reshape(-1, 4)

        obs_cloud = o3d.geometry.PointCloud(points=o3d.utility.Vector3dVector(pc[..., 0:3]))
        obs_cloud.colors = o3d.utility.Vector3dVector(colors[:, 0:3])
        # draw the axis
        coordinate = o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.05, origin=[0, 0, 0])

        o3d.visualization.draw_geometries([obs_cloud, coordinate])
    ic(data.shape)