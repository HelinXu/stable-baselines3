import numpy as np
import open3d as o3d
import matplotlib.pyplot as plt

from icecream import ic, install
install()
ic.configureOutput(includeContext=True, contextAbsPath=True, prefix='File ')

pcs = np.load("../data/faucet_seg_data/test_42.npy")

for i in range(len(pcs)):
    pc = pcs[i]
    labels = pc[..., 3] * 1 + pc[..., 4] * 2 + pc[..., 5] * 3 + pc[..., 6] * 4
    colors = plt.get_cmap("tab20")(labels / 4).reshape(-1, 4)

    obs_cloud = o3d.geometry.PointCloud(points=o3d.utility.Vector3dVector(pc[..., 0:3]))
    obs_cloud.colors = o3d.utility.Vector3dVector(colors[:, 0:3])

    coordinate = o3d.geometry.TriangleMesh.create_coordinate_frame(size=0.05, origin=[0, 0, 0])
    o3d.visualization.draw_geometries([coordinate, obs_cloud])

def view_ply(filename='/home/helin/Code/dexsuite/stable-baselines3/vision_dev/log/sem_seg/2022-08-31_20-13/visual/0_gt.ply'):
    pc = o3d.io.read_point_cloud(filename)
    o3d.visualization.draw_geometries([pc])

if __name__ == '__main__':
    view_ply()