from pathlib import Path

import torch.nn as nn

from hand_env_utils.arg_utils import *
from hand_env_utils.teleop_env import create_faucet_teacher_env
from hand_env_utils.wandb_callback import WandbCallback, setup_wandb
from hand_teleop.real_world import task_setting
from stable_baselines3.common.torch_layers import PointNetExtractor, PointNetImaginationExtractor
from stable_baselines3.common.vec_env.subproc_vec_env import SubprocVecEnv
from stable_baselines3.ppo import PPO
import wandb

from icecream import ic, install
install()
ic.configureOutput(includeContext=True, contextAbsPath=True, prefix='File ')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n', type=int, default=100)
    parser.add_argument('--workers', type=int, default=10)
    parser.add_argument('--lr', type=float, default=3e-4)
    parser.add_argument('--ep', type=int, default=10)
    parser.add_argument('--bs', type=int, default=2000)
    parser.add_argument('--seed', type=int, default=100)
    parser.add_argument('--iter', type=int, default=1000)
    parser.add_argument('--randomness', type=float, default=1.0)
    parser.add_argument('--exp', type=str)
    parser.add_argument('--object_name', type=str, default="faucet")
    parser.add_argument('--object_cat', default="SAPIEN", type=str)
    parser.add_argument('--index', type=int, default=-1)  # -1 means all
    parser.add_argument('--index_group', type=str, default=None)  # top, right, left
    parser.add_argument('--use_bn', dest='use_bn', action='store_true', default=False)
    parser.add_argument('--noise_pc', dest='noise_pc', action='store_true', default=True)
    parser.add_argument('--use_img', dest='use_img', action='store_true', default=False)
    parser.add_argument('--use_seg', dest='use_seg', action='store_true', default=False)
    parser.add_argument('--project', type=str, default='debug')
    parser.add_argument('--pn_version', type=int, default=-1)

    args = parser.parse_args()
    object_name = args.object_name
    object_cat = args.object_cat
    randomness = args.randomness
    use_img = args.use_img
    use_seg = args.use_seg
    pointnet_version = args.pn_version
    if use_img and use_seg:
        raise NotImplementedError("Cannot use both imagination and segmentation")
    noise_pc = args.noise_pc
    project = args.project

    if project != 'debug':
        ic.disable()
        print('ic output disabled.')

    if args.index != -1:
        indeces = [args.index]
        exp_keywords = ['fcent', 'f' + str(args.index),
                        'seg' if use_seg else 'noseg',
                        'img' if use_img else 'noimg',
                        'pn_v' + str(pointnet_version) if pointnet_version > 0 else 'pn_ori',
                        str(args.workers),
                        'bn' if args.use_bn else 'nobn',
                        str(args.seed)]
    else:
        index_group = args.index_group
        if index_group == 'top':
            # indeces = [0, 10, 11, 19, 20, 24, 26, 27, 28, 8]
            indeces = [148, 822, 857, 991]  # 693 is slow
            # unseen_indeces = [34, 38, 41, 42, 44, 47, 53, 55, 61, 46]
            unseen_indeces = [1466, 1556, 1633, 1646, 1667, 1741, 1817, 1832, 1925, 1721]
        else:
            raise NotImplementedError
        exp_keywords = [index_group,
                        'seg' if use_seg else 'noseg',
                        'img' if use_img else 'noimg',
                        'pn_v' + str(pointnet_version) if pointnet_version > 0 else 'pn_ori',
                        str(args.workers),
                        'bn' if args.use_bn else 'nobn',
                        str(args.seed)]
    horizon = 200
    env_iter = args.iter * horizon * args.n

    config = {
        'indeces': indeces,
        'n_env_horizon': args.n,
        'object_name': object_name,
        'object_category': object_cat,
        'update_iteration': args.iter,
        'total_step': env_iter,
        'randomness': randomness,
    }

    exp_name = "-".join(exp_keywords)
    result_path = Path("./results") / exp_name
    result_path.mkdir(exist_ok=True, parents=True)

    wandb_tags = [
        object_name,
        "pose3",
        "ppo",
        "reward1",
        "seg" if use_seg else "no_seg",
        "cam_faucet_side",
        "top" if args.index_group == 'top' and args.index == -1 else str(args.index),
        'img' if use_img else 'no_img',
        'pointnet_v' + str(pointnet_version) if pointnet_version > 0 else 'pointnet',
        'bn' if args.use_bn else 'nobn',
        "faucet_centeric",  # set in task_setting
        ]

    print(f"###############################################")
    print(f"project name:     {project}")
    print(f"exp name:         {exp_name}")
    print(f"wandb tags:       {wandb_tags}")
    print(f"use segmentation: {use_seg}")
    print(f"use imgination:   {use_img}")
    print(f"noise pc:         {noise_pc}")
    print(f"faucet indeces:   {indeces}")
    print(f"pointnet version: {pointnet_version}")
    print(f"use bn:           {args.use_bn}")
    print(f"###############################################\n")


    wandb_run = setup_wandb(config, exp_name, tags=wandb_tags, project=project)

    wandb.save(str(Path(__file__).parent.parent / "hand_teleop/env/rl_env/faucet_teacher_env.py"))
    wandb.save(str(Path(__file__).parent.parent / "hand_teleop/env/sim_env/faucet_env.py"))
    wandb.save(str(Path(__file__).parent.parent / "hand_env_utils/teleop_env.py"))
    wandb.save(str(Path(__file__).parent.parent / "stable_baselines3/networks/pointnet_modules/pointnet.py"))


    def create_env_fn():
        environment = create_faucet_teacher_env(object_name=object_name,
                                                use_visual_obs=True,
                                                use_gui=False,
                                                is_eval=False,
                                                pc_noise=noise_pc,
                                                pc_seg=use_seg,
                                                friction=1,
                                                index=indeces,
                                                use_img=use_img)
        return environment

    def create_eval_env_fn():
        environment = create_faucet_teacher_env(object_name=object_name,
                                                use_visual_obs=True,
                                                use_gui=False,
                                                is_eval=True,
                                                pc_noise=noise_pc,
                                                pc_seg=use_seg,
                                                friction=1,
                                                index=indeces,
                                                use_img=use_img)
        return environment


    env = SubprocVecEnv([create_env_fn] * args.workers, "spawn")  # train on a list of envs.
    # env = create_env_fn()
    # env = SubprocVecEnv([create_env_fn] * args.workers, "spawn")  # train on a list of envs.

    feature_extractor_class = PointNetImaginationExtractor if use_img else PointNetExtractor
    feature_extractor_kwargs = {
        "pc_key": "faucet_1-pc_seg" if use_seg else "faucet_1-point_cloud",
        "local_channels": (64, 128, 256),
        "global_channels": (256,),
        "use_bn": args.use_bn,
    }
    if use_img:
        feature_extractor_kwargs["imagination_keys"] = ('imagination_robot', 'imagination_faucet',)
    if pointnet_version != -1:
        feature_extractor_kwargs["version"] = pointnet_version

    policy_kwargs = {
        "features_extractor_class": feature_extractor_class,
        "features_extractor_kwargs": feature_extractor_kwargs,
        "net_arch": [dict(pi=[64, 64], vf=[64, 64])],
        "activation_fn": nn.ReLU,
    }

    ic(policy_kwargs)
    ic(env.observation_space)

    model = PPO("PointCloudPolicy", env, verbose=1,
                n_epochs=args.ep,
                n_steps=(args.n // args.workers) * horizon,
                learning_rate=args.lr,
                batch_size=args.bs,
                seed=args.seed,
                policy_kwargs=policy_kwargs,
                min_lr=args.lr,
                max_lr=args.lr,
                adaptive_kl=0.02,
                target_kl=0.2,
                )

    if args.index == -1:
        model.learn(
            total_timesteps=int(env_iter),
            callback=WandbCallback(
                model_save_freq=1 if project == 'debug' else 50,
                model_save_path=str(result_path / "model"),
                # eval_env_fns=create_eval_env_fn_list,  # eval a list of envs.
                eval_freq=1 if project == 'debug' else 50,
                eval_env_fn=create_eval_env_fn,  # eval one env.
                eval_cam_names=["faucet_viz"],
                viz_point_cloud=True,
            ),
        )
    else:
        model.learn(
            total_timesteps=int(env_iter),
            callback=WandbCallback(
                model_save_freq=1 if project == 'debug' else 50,
                model_save_path=str(result_path / "model"),
                # eval_env_fns=create_eval_env_fn_list,  # eval a list of envs.
                eval_freq=1 if project == 'debug' else 50,
                eval_env_fn=create_eval_env_fn,  # eval one env.
                eval_cam_names=["faucet_viz"],
                viz_point_cloud=True,
            ),
        )
    wandb_run.finish()
