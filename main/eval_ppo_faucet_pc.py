import numpy as np

from hand_env_utils.teleop_env import create_faucet_teacher_env
from stable_baselines3.ppo import PPO
import cv2
from hand_teleop.utils.camera_utils import fetch_texture, generate_imagination_pc_from_obs
from stable_baselines3.common.torch_layers import PointNetExtractor, PointNetImaginationExtractor
import torch.nn as nn
from icecream import ic

if __name__ == '__main__':
    checkpoint_path = "/checkpoints/model_1800.zip"
    device = "cuda:0"

    for id in [34, 38, 41, 42, 44, 47, 53, 55, 61, 46]:
        video = []
        use_img = False
        use_seg = True
        pointnet_version = 4
        env = create_faucet_teacher_env(object_name='faucet',
                                        use_visual_obs=True,
                                        use_gui=True,
                                        is_eval=True,
                                        pc_noise=True,
                                        use_img=use_img,
                                        pc_seg=use_seg,
                                        friction=1,
                                        index=id)
        feature_extractor_class = PointNetImaginationExtractor if use_img else PointNetExtractor
        feature_extractor_kwargs = {
            "pc_key": "faucet_1-pc_seg" if use_seg else "faucet_1-point_cloud",
            "local_channels": (64, 128, 256),
            "global_channels": (256,),
            "use_bn": False,
        }
        if use_img:
            feature_extractor_kwargs["imagination_keys"] = ('imagination_robot', 'imagination_faucet',)
        if pointnet_version != -1:
            feature_extractor_kwargs["version"] = pointnet_version

        policy_kwargs = {
            "features_extractor_class": feature_extractor_class,
            "features_extractor_kwargs": feature_extractor_kwargs,
            "net_arch": [dict(pi=[64, 64], vf=[64, 64])],
            "activation_fn": nn.ReLU,
        }

        ic(policy_kwargs)
        policy = PPO.load(checkpoint_path, env, device, policy_kwargs=policy_kwargs)
        reward_sum = 0
        obs = env.reset()
        for i in range(env.horizon):
            action = policy.predict(observation=obs, deterministic=True)[0]
            obs, reward, done, _ = env.step(action)
            env.scene.update_render()

            cam_name = 'faucet_viz'
            cam = env.cameras[cam_name]
            cam.take_picture()
            video.append(fetch_texture(cam, "Color", return_torch=False))

            reward_sum += reward

        video_array = (np.stack(video, axis=0) * 255).astype(np.uint8)
        # video_array = np.transpose(video_array, (0, 1, 2))
        ic(video_array.shape)
        out = cv2.VideoWriter(f"videos/seen_{id}_{int(reward_sum)}.mp4", cv2.VideoWriter_fourcc(*'mp4v'), 20,
                              (video_array.shape[1], video_array.shape[2]))
        for frame in video_array:
            frame = cv2.cvtColor(frame, cv2.COLOR_RGBA2BGR)
            # frame = cv2.resize(frame, (240, 240, 3))
            out.write(frame)  # frame is a numpy.ndarray with shape (1280, 720, 3)
        out.release()
