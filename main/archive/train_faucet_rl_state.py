from pathlib import Path

import torch.nn as nn
import wandb

from hand_env_utils.arg_utils import *
from hand_env_utils.teleop_env import create_faucet_teacher_env
from hand_env_utils.wandb_callback import WandbCallback, setup_wandb
from stable_baselines3.common.vec_env.subproc_vec_env import SubprocVecEnv
from stable_baselines3.ppo import PPO

from icecream import ic, install
install()
ic.configureOutput(includeContext=True, contextAbsPath=True)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n', type=int, default=100)
    parser.add_argument('--workers', type=int, default=10)
    parser.add_argument('--lr', type=float, default=3e-4)
    parser.add_argument('--ep', type=int, default=10)
    parser.add_argument('--bs', type=int, default=2000)
    parser.add_argument('--seed', type=int, default=100)
    parser.add_argument('--iter', type=int, default=1000)
    parser.add_argument('--randomness', type=float, default=1.0)
    parser.add_argument('--exp', type=str)
    parser.add_argument('--object_name', type=str)
    parser.add_argument('--object_cat', default="SAPIEN", type=str)
    parser.add_argument('--index', type=int, default=-1)  # -1 means all

    args = parser.parse_args()
    object_name = args.object_name
    index = args.index
    object_cat = args.object_cat
    randomness = args.randomness
    exp_keywords = ["ppo_teacher_state", object_name + "_" + str(index), args.exp, str(args.seed)]
    horizon = 200
    env_iter = args.iter * horizon * args.n

    config = {
        'index': index,
        'n_env_horizon': args.n,
        'object_name': object_name,
        'object_category': object_cat,
        'update_iteration': args.iter,
        'total_step': env_iter,
        'randomness': randomness,
    }

    exp_name = "-".join(exp_keywords)
    result_path = Path("./results") / exp_name
    result_path.mkdir(exist_ok=True, parents=True)
    wandb_run = setup_wandb(config, exp_name, tags=["state", "faucet", object_name, "naive-rl", "teacher1", "pose1"], project='teacher-faucet')

    wandb.save(str(Path(__file__).parent.parent / "hand_teleop/env/rl_env/faucet_teacher_env.py"))
    wandb.save(str(Path(__file__).parent.parent / "hand_teleop/env/sim_env/faucet_env.py"))
    wandb.save(str(Path(__file__).parent.parent / "hand_env_utils/teleop_env.py"))

    def create_env_fn():
        environment = create_faucet_teacher_env(object_name=object_name,
                              use_visual_obs=False,
                              use_gui=False,
                              is_eval=False,
                              pc_noise=True,
                              friction=1,
                              index=index)
        return environment


    def create_eval_env_fn():
        environment = create_faucet_teacher_env(object_name=object_name,
                              use_visual_obs=False,
                              use_gui=False,
                              is_eval=True,
                              pc_noise=True,
                              friction=1,
                              index=index)
        return environment

    env = SubprocVecEnv([create_env_fn] * args.workers, "spawn")

    print(env.observation_space, env.action_space)

    model = PPO("MlpPolicy", env, verbose=1,
                n_epochs=args.ep,
                n_steps=(args.n // args.workers) * horizon,
                learning_rate=args.lr,
                batch_size=args.bs,
                seed=args.seed,
                policy_kwargs={'activation_fn': nn.ReLU},
                min_lr=args.lr,
                max_lr=args.lr,
                adaptive_kl=0.02,
                target_kl=0.2,
                )

    model.learn(
        total_timesteps=int(env_iter),
        callback=WandbCallback(
            model_save_freq=50,
            model_save_path=str(result_path / "model"),
            eval_env_fn=create_eval_env_fn,
            eval_freq=50,
            eval_cam_names=["faucet_viz"],
            # viz_point_cloud=True,
        ),
    )
    wandb_run.finish()
