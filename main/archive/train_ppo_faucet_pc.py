from pathlib import Path

import torch.nn as nn

from hand_env_utils.arg_utils import *
from hand_env_utils.teleop_env import create_faucet_teacher_env
from hand_env_utils.wandb_callback import WandbCallback, setup_wandb
from hand_teleop.real_world import task_setting
from stable_baselines3.common.torch_layers import PointNetStateExtractor
from stable_baselines3.common.vec_env.subproc_vec_env import SubprocVecEnv
from stable_baselines3.ppo import PPO
import wandb

from icecream import ic, install
install()
ic.configureOutput(includeContext=True, contextAbsPath=True)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--n', type=int, default=100)
    parser.add_argument('--workers', type=int, default=10)
    parser.add_argument('--lr', type=float, default=3e-4)
    parser.add_argument('--ep', type=int, default=10)
    parser.add_argument('--bs', type=int, default=2000)
    parser.add_argument('--seed', type=int, default=100)
    parser.add_argument('--iter', type=int, default=1000)
    parser.add_argument('--randomness', type=float, default=1.0)
    parser.add_argument('--exp', type=str)
    parser.add_argument('--object_name', type=str, default="faucet")
    parser.add_argument('--object_cat', default="SAPIEN", type=str)
    parser.add_argument('--index', type=int, default=-1)  # -1 means all
    parser.add_argument('--index_group', type=str, default=None)  # top, right, left
    parser.add_argument('--use_bn', type=bool, default=True)
    parser.add_argument('--noise_pc', type=bool, default=True)

    args = parser.parse_args()
    object_name = args.object_name
    object_cat = args.object_cat
    randomness = args.randomness
    if args.index_group is None:
        indeces = [args.index]
        exp_keywords = ["ppo_pc_noimg", object_name + "_" + str(indeces[0]), args.exp,
                        "lr" + str(args.lr), "bs" + str(args.bs), str(args.seed)]
    else:
        index_group = args.index_group
        if index_group == 'top':
            indeces = [0, 8, 10, 11, 19, 20, 24, 26, 27, 28]
            unseen_indeces = [34, 38, 41, 42, 44, 47, 53, 55, 61, 46]
        else:
            raise NotImplementedError
        exp_keywords = ["ppo_pc_noimg", object_name + "_" + index_group, args.exp,
                        "lr" + str(args.lr), "bs" + str(args.bs), str(args.seed)]
    horizon = 200
    env_iter = args.iter * horizon * args.n

    config = {
        'indeces': indeces,
        'n_env_horizon': args.n,
        'object_name': object_name,
        'object_category': object_cat,
        'update_iteration': args.iter,
        'total_step': env_iter,
        'randomness': randomness,
    }

    exp_name = "-".join(exp_keywords)
    result_path = Path("./results") / exp_name
    result_path.mkdir(exist_ok=True, parents=True)
    wandb_run = setup_wandb(config, exp_name, tags=[object_name, "pose3", "ppo", "reward1", "cam_faucet_1", "top"], project="generalize")

    wandb.save(str(Path(__file__).parent.parent / "hand_teleop/env/rl_env/faucet_teacher_env.py"))
    wandb.save(str(Path(__file__).parent.parent / "hand_teleop/env/sim_env/faucet_env.py"))
    wandb.save(str(Path(__file__).parent.parent / "hand_env_utils/teleop_env.py"))


    def create_env_fn(index=indeces[0]):
        environment = create_faucet_teacher_env(object_name=object_name,
                                                use_visual_obs=True,
                                                use_gui=False,
                                                is_eval=False,
                                                pc_noise=True,
                                                friction=1,
                                                index=index)
        return environment

    def create_env_fn_list():
        env_fn_list = []
        for idx in range(args.workers):
            def create_env_fn_i(idx=idx):
                environment = create_faucet_teacher_env(object_name=object_name,
                                                        use_visual_obs=True,
                                                        use_gui=False,
                                                        is_eval=False,
                                                        pc_noise=True,
                                                        friction=1,
                                                        index=indeces[idx])
                return environment
            env_fn_list.append(create_env_fn_i)
        return env_fn_list

    def create_eval_env_fn(index=indeces[0]):
        environment = create_faucet_teacher_env(object_name=object_name,
                                                use_visual_obs=True,
                                                use_gui=False,
                                                is_eval=True,
                                                pc_noise=True,
                                                friction=1,
                                                index=index)
        return environment


    def create_eval_env_fn_list():
        env_fn_list = []
        for idx in range(args.workers):
            def create_eval_env_fn_i(idx=idx):
                environment = create_faucet_teacher_env(object_name=object_name,
                                                        use_visual_obs=True,
                                                        use_gui=False,
                                                        is_eval=True,
                                                        pc_noise=True,
                                                        friction=1,
                                                        index=indeces[idx])
                return environment
            env_fn_list.append(create_eval_env_fn_i)
        # for idx in unseen_indeces:
        #     def create_eval_env_fn_i(idx=idx):
        #         environment = create_faucet_teacher_env(object_name=object_name,
        #                                                 use_visual_obs=True,
        #                                                 use_gui=False,
        #                                                 is_eval=True,
        #                                                 pc_noise=True,
        #                                                 friction=1,
        #                                                 index=idx)
        #         return environment
        #     env_fn_list.append(create_eval_env_fn_i)
        return env_fn_list


    env = SubprocVecEnv(create_env_fn_list(), "spawn")

    feature_extractor_class = PointNetStateExtractor
    feature_extractor_kwargs = {
        "pc_key": "faucet_1-point_cloud",  # RuntimeError: Point cloud key faucet-point_cloud not in observation space.
        "local_channels": (64, 128, 256),
        "global_channels": (256,),
        "use_bn": args.use_bn,
        "state_mlp_size": (64, 64),
    }
    policy_kwargs = {
        "features_extractor_class": feature_extractor_class,
        "features_extractor_kwargs": feature_extractor_kwargs,
        "net_arch": [dict(pi=[64, 64], vf=[64, 64])],
        "activation_fn": nn.ReLU,
    }

    config = {'n_env_horizon': args.n, 'object_name': args.object_name, 'update_iteration': args.iter,
              'total_step': env_iter, "use_bn": args.use_bn, "policy_kwargs": policy_kwargs}

    model = PPO("PointCloudPolicy", env, verbose=1,
                n_epochs=args.ep,
                n_steps=(args.n // args.workers) * horizon,
                learning_rate=args.lr,
                batch_size=args.bs,
                seed=args.seed,
                policy_kwargs=policy_kwargs,
                min_lr=args.lr,
                max_lr=args.lr,
                adaptive_kl=0.02,
                target_kl=0.2,
                )

    model.learn(
        total_timesteps=int(env_iter),
        callback=WandbCallback(
            model_save_freq=50,
            model_save_path=str(result_path / "model"),
            eval_env_fns=create_eval_env_fn_list,
            eval_freq=50,
            eval_cam_names=["faucet_viz"],
            viz_point_cloud=True,
        ),
    )
    wandb_run.finish()
