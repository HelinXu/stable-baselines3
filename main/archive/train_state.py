# Used to train/resume/visualize a model.
# Written by Helin Xu, May. 12, 2022

import sys
from stable_baselines3 import PPO
import sys
from pathlib import Path
sys.path.append(str(Path(__file__).resolve().parents[1]))
from configs.config import assert_cfg, cfg
import numpy as np
import argparse
# from tqdmś import tqdm
from stable_baselines3.common.vec_env import SubprocVecEnv
from stable_baselines3.common.monitor import Monitor
import torch
from hand_teleop.env.rl_env.relocate_env import RelocateRLEnv
import os
import os.path as osp
import cv2
import time
import tensorboard
from icecream import install, ic
import wandb
install()
ic.configureOutput(includeContext=True)


class Spec:
    def __init__(self, env=None, env_name="relocate-mug-1"):
        self.observation_dim = env.env.obs_dim
        self.action_dim = env.env.action_dim
        self.env_id = env_name


def parse_args():
    """Parses the arguments."""
    parser = argparse.ArgumentParser(
        description='Train agent from states'
    )
    parser.add_argument(
        '--cfg',
        dest='cfg_file',
        help='Config file',
        required=True,
        type=str
    )
    parser.add_argument(
        'opts',
        help='See pycls/core/config.py for all options',
        default=None,
        nargs=argparse.REMAINDER
    )
    parser.add_argument(
        '--resume',
        dest='resume',
        help='Resume training from the given model',
        action='store_true'
    )
    parser.add_argument(
        '--visualize',
        dest='visualize',
        help='Visualize the given model',
        action='store_true'
    )
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)
    return parser.parse_args()


def train(resu, visu):
    # TODO: helin: this is not applicable for all environments
    # applicable for relocate, rearrange, door.

    task_name = cfg.TASK_NAME
    robot_name = cfg.ROBOT_NAME
    object_name = cfg.OBJECT_NAME
    object_scale = cfg.OBJECT_SCALE
    num_cpu = cfg.NUM_CPU
    resume = cfg.RESUME or resu
    videopath = os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED)) if cfg.VIDEO_PATH == '' else cfg.VIDEO_PATH
    rotation_reward_weight = 0
    tensorboard_log = os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED))
    visualize = cfg.VISUALIZE or visu
    if resume or visualize:
        with open(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "last_checkpoint.txt"), "r") as f:
            checkpoint_name = f.read()
        resume_checkpoint_path = os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), checkpoint_name + '.zip')  # JOB_DIR/RNG_SEED/CHECKPOINT_NAME.zip
        visualize_checkpoint_path = resume_checkpoint_path if cfg.CHECKPOINT_NAME == '' else os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), cfg.CHECKPOINT_NAME + '.zip')  # JOB_DIR/RNG_SEED/CHECKPOINT_NAME
    use_visual_obs = cfg.USE_VISUAL_OBS

    env_params = dict(object_name=object_name, robot_name=robot_name, rotation_reward_weight=rotation_reward_weight,
                      constant_object_state=False, randomness_scale=1, use_visual_obs=use_visual_obs, use_gui=visualize)

    if not visualize:
        wandb.init(project=cfg.WANDB.PROJECT, sync_tensorboard=True, entity='helinxu', name=cfg.WANDB.NAME, resume=resume)
    # backup config file
    ic(cfg)
    if not os.path.exists(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED))):
        os.makedirs(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED)))
    if visualize:
        with open(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "config_visualize_{}.yaml".format(time.time())), "w") as f:
            f.write(cfg.dump())
    elif resume:
        with open(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "config_resume_{}.yaml".format(time.time())), "w") as f:
            f.write(cfg.dump())
    else:
        with open(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "config_train_{}.yaml".format(time.time())), "w") as f:
            f.write(cfg.dump())
        wandb.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "config_train_*.yaml"))

    # Specify rendering device if the computing device is given
    if "CUDA_VISIBLE_DEVICES" in os.environ:
        device = f"cuda:{os.environ['CUDA_VISIBLE_DEVICES']}"
        env_params["device"] = device
        ic(device)
    if task_name == "rearrange" or task_name == "relocate":
        ic(env_params)
        if visualize:
            env = RelocateRLEnv(**env_params)
        else:
            env = SubprocVecEnv(([lambda: Monitor(RelocateRLEnv(**env_params))]*num_cpu))
            #env = RelocateRLEnv(**env_params)
            # start = time.time()
            # for _ in tqdm(range(10000)):
            #     env.step(np.array([env.action_space.sample()]*8))
            # print(f'fps: {80000/(time.time() - start)}')
            # exit()
    else:
        raise NotImplementedError

    # Do not create cameras here

    env.reset()

    # If visualize, do not further train.
    if visualize:
        print("Visualizing...")

        horizon = 300
        model = PPO.load(visualize_checkpoint_path, env)
        video_dir = os.path.join(videopath, "video")
        if not os.path.exists(video_dir):
            os.makedirs(video_dir)
        for n in range(1, 30):
            o = env.reset()
            d = False
            t = 0
            score = 0.0

            while t < horizon and d is False:
                # print(t)
                a, _s = model.predict(o)
                o, r, d, _ = env.step(a)
                t = t + 1
                score = score + r

                viewer = env.render()
                rgba = viewer.window.get_float_texture('Color')
                image = (rgba * 255).clip(0, 255).astype("uint8")
                image = cv2.cvtColor(image, cv2.COLOR_RGBA2RGB)
                image = image[..., ::-1]

                if t <= 1:
                    video_file = osp.join(video_dir, f'{task_name}_step={(visualize_checkpoint_path.split(".")[-2]).split("/")[-1]}_seed={cfg.RNG_SEED}_{n}.mp4')
                    print(video_file)
                    video_writer = cv2.VideoWriter(video_file, cv2.VideoWriter_fourcc(*'mp4v'), 20,
                                                   (image.shape[1], image.shape[0]), isColor=True)
                video_writer.write(image)
                if n < 2:
                    time.sleep(.1)  # TODO

            video_writer.release()
            print("Episode score = %f" % score)
        env.close()
        return

    # resume or not
    starting_steps = 0
    if resume:
        print("Resuming training...")
        model = PPO.load(resume_checkpoint_path, env, tensorboard_log=tensorboard_log, batch_size=cfg.PPO.BATCH_SIZE)
        print("Resumed from checkpoint {}".format(resume_checkpoint_path))
        starting_steps = int(checkpoint_name)
    else:
        print("Starting training...")
        model = PPO(cfg.PPO.POLICY, env, verbose=1,
                    tensorboard_log=tensorboard_log, batch_size=cfg.PPO.BATCH_SIZE, seed=cfg.RNG_SEED)

    # Train model
    TIMESTEPS = cfg.TIMESTEPS
    total_steps = starting_steps
    # at first, we want to save more often
    small_start_steps = 1000
    if not os.path.exists(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED))):
        os.makedirs(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED)))
    if total_steps < small_start_steps:
        print(f'Starting with {small_start_steps} steps...')
        model.learn(total_timesteps=small_start_steps,
                    reset_num_timesteps=False, tb_log_name=f"tb")
        total_steps = small_start_steps
        print(f"Saving to {os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), str(total_steps))}")
        model.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), str(total_steps)))
        with open(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "last_checkpoint.txt"), "w") as f:
            f.write(str(total_steps))
        wandb.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED),  str(total_steps)+'.zip'))


    while 2 * total_steps <= TIMESTEPS:
        print(f"Total steps done: {total_steps}")
        model.learn(total_timesteps=total_steps,
                    reset_num_timesteps=False, tb_log_name=f"tb")
        total_steps *= 2
        print(f"Saving to {os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), str(total_steps))}")
        model.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), str(total_steps)))
        with open(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "last_checkpoint.txt"), "w") as f:
            f.write(str(total_steps))
        wandb.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED),  str(total_steps)+'.zip'))


    assert 2 * total_steps > TIMESTEPS

    if total_steps < TIMESTEPS:
        print(f"Total steps done: {total_steps}")
        model.learn(total_timesteps=TIMESTEPS - total_steps,
                    reset_num_timesteps=False, tb_log_name=f"tb")
        total_steps = TIMESTEPS
        print(f"Saving to {os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), str(total_steps))}")
        model.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), str(total_steps)))
        with open(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "last_checkpoint.txt"), "w") as f:
            f.write(str(total_steps))
        wandb.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED),  str(total_steps)+'.zip'))


    while total_steps < cfg.TOTAL_STEPS:
        print(f"Total steps: {total_steps}")
        model.learn(total_timesteps=TIMESTEPS,
                    reset_num_timesteps=False, tb_log_name=f"tb")
        total_steps += TIMESTEPS
        print(f"Saving to {os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), str(total_steps))}")
        model.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), str(total_steps)))
        with open(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED), "last_checkpoint.txt"), "w") as f:
            f.write(str(total_steps))
        wandb.save(os.path.join(cfg.JOB_DIR, str(cfg.RNG_SEED),  str(total_steps)+'.zip'))

        # obs = env.reset()
        # for _ in range(100):
        #     act = model.policy(obs)[0]
        #     obs, rew, done, info = model.env.step(act)
        #     render_array = model.env.render(mode='rgb_array')
        # wandb.log({'video': wandb.Video(render_array)})


if __name__ == '__main__':
    # Load the config
    args = parse_args()
    cfg.merge_from_file(args.cfg_file)
    cfg.merge_from_list(args.opts)
    assert_cfg()
    cfg.freeze()
    # Train the agent
    train(args.resume, args.visualize)
